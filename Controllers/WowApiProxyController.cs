﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace wow_ggm_backend.Controllers
{
    [ApiController]
    [EnableCors]
    [Route("wow/")]
    public class WowApiProxyController : ControllerBase
    {
        private static Token? token;
        private readonly ILogger<WowApiProxyController> logger;

        public WowApiProxyController(ILogger<WowApiProxyController> logger)
        {
            this.logger = logger;
        }

        [HttpGet("{escapedUrl}")]
        public async Task<IActionResult> Proxy(string escapedUrl)
        {
            string url = Uri.UnescapeDataString(escapedUrl);
            logger.LogDebug("Proxying URL {Url}", url);

            // if the token expires in less than 5 minutes
            if (token is null || token.ExpiresAt.Subtract(DateTime.Now).TotalSeconds < 300)
            {
                logger.LogDebug("Refreshing OAuth token");
                try
                {
                    token = await GetOauthToken();
                }
                catch (Exception e) when (e is HttpRequestException ex)
                {
                    return ex.ToActionResult(401);
                }
            }

            string accessToken = token.TokenObject.access_token;

            logger.LogDebug("Using token {AccessToken}", accessToken);

            logger.LogDebug("Sending WoW API request");

            using HttpClient client = new();
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");

            try
            {
                string json = await client.GetStringAsync(url);

                logger.LogDebug("Got response");

                return Ok(json);
            }
            catch (Exception e) when (e is InvalidOperationException)
            {
                return BadRequest(e.ToString());
            }
            catch (Exception e) when (e is HttpRequestException ex)
            {
                return ex.ToActionResult(400);
            }
        }

        private static async Task<Token> GetOauthToken()
        {
            const string tokenUrl = "https://us.battle.net/oauth/token";
            using HttpClient tokenClient = new();
            using MultipartFormDataContent form = new() {{new StringContent("client_credentials"), "grant_type"}};
            string authHeader =
                $"Basic {Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes("e1af0d892f104d88b3153235744a432d:CYpyi6hT83yTematHH8MoL8DTZU5jAeD"))}";
            tokenClient.DefaultRequestHeaders.Add("Authorization", authHeader);

            using HttpResponseMessage tokenResponse = await tokenClient.PostAsync(new Uri(tokenUrl, UriKind.Absolute), form);
            string tokenJson = await tokenResponse.EnsureSuccessStatusCode().Content.ReadAsStringAsync();

            TokenObject tokenObject = JsonSerializer.Deserialize<TokenObject>(tokenJson)!;

            return new Token(tokenObject);
        }

        [SuppressMessage("ReSharper", "ClassNeverInstantiated.Local")]
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        [SuppressMessage("ReSharper", "UnusedMember.Local")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
        private class TokenObject
        {
            public string access_token { get; set; } = null!;
            public string token_type { get; set; } = null!;
            public int expires_in { get; set; }
            public string scope { get; set; } = null!;
        }

        private class Token
        {
            public Token(TokenObject tokenObject)
            {
                TokenObject = tokenObject;
                ExpiresAt = DateTime.Now.AddSeconds(tokenObject.expires_in);
            }

            public TokenObject TokenObject { get; }
            public DateTime ExpiresAt { get; }
        }
    }
}
