﻿using System.Net.Http;
using Microsoft.AspNetCore.Mvc;

namespace wow_ggm_backend.Controllers
{
    public static class ExceptionExtensions
    {
        public static IActionResult ToActionResult(this HttpRequestException exception, int defaultStatusCode)
        {
            return new ObjectResult(exception.ToString()) {StatusCode = exception.StatusCode.HasValue ? (int) exception.StatusCode.Value : defaultStatusCode};
        }
    }
}
